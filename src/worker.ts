
const workerCode =`import { parentPort } from 'worker_threads';
parentPort.on('message', async (param) => {
  try {
    process.stdout.write(JSON.stringify(param)+'\\n');
  } catch (error) {
    parentPort.postMessage('error logging message:' + error);
  }
});`;

export const workerURL = new URL(`data:application/javascript;base64,${Buffer.from(workerCode).toString('base64')}`);