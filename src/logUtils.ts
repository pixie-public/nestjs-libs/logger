// thanks to: https://raw.githubusercontent.com/sevensc/typescript-string-operations/master/dist/index.ts
// avoid another npm dependency

import { Logger } from './logger.service';

export class LogUtils {
  private static readonly regexNumber = /{(\d+(:\w*)?)}/g;
  private static readonly regexObject = /{(\w+(:\w*)?)}/g;

  public static Empty = '';

  public static SafeStringify(obj: any): string {
    const cache: Set<any> = new Set();
    return JSON.stringify(obj, (key, value) => {
      if (typeof value === 'object' && value !== null) {
        if (cache.has(value)) {
          return;
        }
        cache.add(value);
      }
      return value;
    });
  }


  public static IsNullOrWhiteSpace(value: string): boolean {
    try {
      if (value == null || value == 'undefined') {
        return true;
      }

      return value.toString().replace(/\s/g, '').length < 1;
    } catch (e) {
      console.log(e);
      return false;
    }
  }

  public static Join(
    delimiter: string,
    ...args: (string | any | Array<any>)[]
  ): string {
    try {
      const firstArg = args[0];
      if (Array.isArray(firstArg) || firstArg instanceof Array) {
        let tempString = LogUtils.Empty;
        const count = 0;

        for (let i = 0; i < firstArg.length; i++) {
          const current = firstArg[i];
          if (i < firstArg.length - 1) {
            tempString += current + delimiter;
          } else {
            tempString += current;
          }
        }

        return tempString;
      } else if (typeof firstArg === 'object') {
        let tempString = LogUtils.Empty;
        const objectArg = firstArg;
        const keys = Object.keys(firstArg); //get all Properties of the Object as Array
        keys.forEach(element => {
          tempString += (<any>objectArg)[element] + delimiter;
        });
        tempString = tempString.slice(0, tempString.length - delimiter.length); //remove last delimiter
        return tempString;
      }

      const stringArray = <string[]>args;

      return LogUtils.join(delimiter, ...stringArray);
    } catch (e) {
      console.log(e);
      return LogUtils.Empty;
    }
  }

  public static Format(format: string, ...args: any[]): string {
    if (format === undefined) return '';

    try {
      if (format.match(LogUtils.regexNumber)) {
        return LogUtils.format(LogUtils.regexNumber, format, args);
      }

      if (format.match(LogUtils.regexObject)) {
        return LogUtils.format(LogUtils.regexObject, format, args, true);
      }

      return LogUtils.join(' ', format, ...args);
    } catch (e) {
      Logger.warn(`Unable to format string ${format}`);
      return LogUtils.Empty;
    }
  }

  public static Sanitize(value: any): any {
    if (typeof value === 'object' && value !== null) {
      const sanitized = { ...value };
      if (Object.keys(sanitized).length === 0)
        return value

      if ('password' in sanitized) {
        sanitized.password = '--omitted--';
      }
      return sanitized;
    }

    return value;
  }

  private static format(
    regex: any,
    format: string,
    args: any,
    parseByObject = false,
  ): string {
    return format.replace(regex, function(match, x) {
      //0
      const s = match.split(':');
      if (s.length > 1) {
        x = s[0].replace('{', '');
        match = s[1].replace('}', ''); //U
      }

      let arg = null;
      if (parseByObject) {
        arg = args[0][x];
      } else {
        arg = args[x];
      }

      if (arg instanceof Object) {
        const san = LogUtils.Sanitize(arg)
        arg = JSON.stringify(san)
      }
      if (arg === null || match.match(/{\d+}/)) {
        return arg;
      }

      arg = LogUtils.parsePattern(match, arg);
      return typeof arg != 'undefined' && arg != null ? arg : LogUtils.Empty;
    });
  }

  private static parsePattern(
    match: 'L' | 'U' | 'd' | 's' | 'n' | 'x' | 'X' | string,
    arg: string | Date | number | any,
  ): string {
    switch (match) {
      case 'L': {
        arg = arg.toLocaleLowerCase();
        return arg;
      }
      case 'U': {
        arg = arg.toLocaleUpperCase();
        return arg;
      }
      case 'd': {
        if (typeof arg === 'string') {
          return LogUtils.getDisplayDateFromString(arg);
        } else if (arg instanceof Date) {
          return LogUtils.Format(
            '{0:00}.{1:00}.{2:0000}',
            arg.getDate(),
            arg.getMonth(),
            arg.getFullYear(),
          );
        }
        break;
      }
      case 's': {
        if (typeof arg === 'string') {
          return LogUtils.getSortableDateFromString(arg);
        } else if (arg instanceof Date) {
          return LogUtils.Format(
            '{0:0000}-{1:00}-{2:00}',
            arg.getFullYear(),
            arg.getMonth(),
            arg.getDate(),
          );
        }
        break;
      }
      case 'n': {
        //Tausender Trennzeichen
        if (typeof arg !== 'string') arg = arg.toString();
        const replacedString = arg.replace(/,/g, '.');
        if (isNaN(parseFloat(replacedString)) || replacedString.length <= 3) {
          break;
        }

        const numberparts = replacedString.split(/[^0-9]+/g);
        let parts = numberparts;

        if (numberparts.length > 1) {
          parts = [
            LogUtils.join('', ...numberparts.splice(0, numberparts.length - 1)),
            numberparts[numberparts.length - 1],
          ];
        }

        const integer = parts[0];

        const mod = integer.length % 3;
        let output = mod > 0 ? integer.substring(0, mod) : LogUtils.Empty;
        const firstGroup = output;
        const remainingGroups = integer.substring(mod).match(/.{3}/g);
        output = output + '.' + LogUtils.Join('.', remainingGroups);
        arg = output + (parts.length > 1 ? ',' + parts[1] : '');
        return arg;
      }
      case 'x': {
        return this.decimalToHexString(arg);
      }
      case 'X': {
        return this.decimalToHexString(arg, true);
      }
      default: {
        break;
      }
    }

    if (
      (typeof arg === 'number' || !isNaN(arg)) &&
      !isNaN(+match) &&
      !LogUtils.IsNullOrWhiteSpace(arg)
    ) {
      return LogUtils.formatNumber(arg, match);
    }

    return arg;
  }

  private static decimalToHexString(value: string, upperCase = false) {
    const parsed = parseFloat(value as string);
    const hexNumber = parsed.toString(16);
    return upperCase ? hexNumber.toLocaleUpperCase() : hexNumber;
  }

  private static getDisplayDateFromString(input: string): string {
    let splitted: string[];
    splitted = input.split('-');

    if (splitted.length <= 1) {
      return input;
    }

    let day = splitted[splitted.length - 1];
    const month = splitted[splitted.length - 2];
    const year = splitted[splitted.length - 3];
    day = day.split('T')[0];
    day = day.split(' ')[0];

    return `${day}.${month}.${year}`;
  }

  private static getSortableDateFromString(input: string): string {
    const splitted = input.replace(',', '').split('.');
    if (splitted.length <= 1) {
      return input;
    }

    const times = splitted[splitted.length - 1].split(' ');
    let time = LogUtils.Empty;
    if (times.length > 1) {
      time = times[times.length - 1];
    }

    const year = splitted[splitted.length - 1].split(' ')[0];
    const month = splitted[splitted.length - 2];
    const day = splitted[splitted.length - 3];
    let result = `${year}-${month}-${day}`;

    if (!LogUtils.IsNullOrWhiteSpace(time) && time.length > 1) {
      result += `T${time}`;
    } else {
      result += 'T00:00:00';
    }

    return result;
  }

  private static formatNumber(input: number, formatTemplate: string): string {
    const count = formatTemplate.length;
    const stringValue = input.toString();
    if (count <= stringValue.length) {
      return stringValue;
    }

    let remainingCount = count - stringValue.length;
    remainingCount += 1; //Array must have an extra entry

    return new Array(remainingCount).join('0') + stringValue;
  }

  private static join(delimiter: string, ...args: string[]): string {
    let temp = LogUtils.Empty;
    for (let i = 0; i < args.length; i++) {
      if (
        (typeof args[i] == 'string' && LogUtils.IsNullOrWhiteSpace(args[i])) ||
        (typeof args[i] != 'number' && typeof args[i] != 'string')
      ) {
        continue;
      }

      const arg = '' + args[i];
      temp += arg;
      for (let i2 = i + 1; i2 < args.length; i2++) {
        if (LogUtils.IsNullOrWhiteSpace(args[i2])) {
          continue;
        }

        temp += delimiter;
        i = i2 - 1;
        break;
      }
    }

    return temp;
  }
}

export class StringBuilder {
  public Values: string[];

  constructor(value?: string) {
    this.Values = [];

    if (!LogUtils.IsNullOrWhiteSpace(value)) {
      this.Values = new Array(value);
    }
  }

  public ToString() {
    return this.Values.join('');
  }

  public Append(value: string) {
    this.Values.push(value);
  }

  public AppendLine(value: string) {
    this.Values.push('\r\n' + value);
  }

  public AppendFormat(format: string, ...args: any[]) {
    this.Values.push(LogUtils.Format(format, ...args));
  }

  public AppendLineFormat(format: string, ...args: any[]) {
    this.Values.push('\r\n' + LogUtils.Format(format, ...args));
  }

  public Clear() {
    this.Values = [];
  }
}
