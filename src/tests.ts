import { JsonLogger, LoggerInterface } from "./logger.service";

const logger: LoggerInterface = new JsonLogger()

function sleep(ms: number): Promise<void> {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }
  
async function exec() {
    for (let index = 0; index < 1000; index++) {
        logger
          .with('b', {b:0})
          .with('a', {password: 'remove pass'})
          .log("test log {0} {1} {2}",index, {b:'asd', password: 'remove?'}, new Date());
        await sleep(100);
    }        
}

exec()
console.log("EXITED")