import { Injectable, LoggerService } from '@nestjs/common';
import { InnerJsonLog } from "./innerLogger.service";
import { LogUtils } from "./logUtils";
import { Worker } from "worker_threads";
import { workerURL } from './worker';

export enum LogLevelEnum {
  ERROR,
  WARN,
  LOG,
  DEBUG,
}

export interface LoggerInterface extends LoggerService {
  with(field: string, value: any): LoggerInterface;
}

// internal interface to pass into InnerLogger, on with function
export interface JsonLoggerInterface extends LoggerService {
  json(param: any);
}

@Injectable()
export class JsonLogger implements JsonLoggerInterface, LoggerInterface {
  protected readonly logLevel: LogLevelEnum;
  protected readonly worker: Worker;

  constructor(
    protected app: string = JsonLogger.name,
    protected scope: string = 'static-logger',
    protected uid: string = null,
    logLevel: LogLevelEnum | string = 'LOG',
    useWorkerThread: boolean = false,
  ) {

    if (!this.app) {
      this.app = JsonLogger.name;
    }

    if (!this.scope) {
      this.scope = 'static-logger';
    }

    if (typeof logLevel === 'string'){
      switch (logLevel) {
        case 'ERROR':
          this.logLevel = LogLevelEnum.ERROR;
          break;
        case 'WARN':
          this.logLevel = LogLevelEnum.WARN;
          break;
        case 'LOG':
          this.logLevel = LogLevelEnum.LOG;
        case 'DEBUG':
          this.logLevel = LogLevelEnum.DEBUG;
          break
        default:
          this.logLevel = LogLevelEnum.LOG;
          this.log("log level unknown. using LogLevelEnum.LOG")
      }
    } else {
      this.logLevel = logLevel;
    }

    this.worker = null;
    if (useWorkerThread) {
      try {
        this.warn("logger service with worker thread" );
        this.worker = new Worker(workerURL, { eval: false });
        this.worker.on('message', (message) => {
          console.warn(message);
        });
      } catch(e) {
        this.worker = null;
        this.with("exception", e).error("unable to create Logger worker. using default console log. " );
      }
    }
  }

  log(format: string, ...optionalParams: any[]) {
    if (this.logLevel >= LogLevelEnum.LOG)
      this.json({
        level: LogLevelEnum.LOG,
        message: LogUtils.Format(format, ...optionalParams),
      });
  }

  error(format: string, ...optionalParams: any[]) {
    if (this.logLevel >= LogLevelEnum.ERROR)
      this.json({
        level: LogLevelEnum.ERROR,
        message: LogUtils.Format(format, ...optionalParams),
      });
  }

  warn(format: string, ...optionalParams: any[]) {
    if (this.logLevel >= LogLevelEnum.WARN)
      this.json({
        level: LogLevelEnum.WARN,
        message: LogUtils.Format(format, ...optionalParams),
      });
  }

  debug?(format: string, ...optionalParams: any[]) {
    if (this.logLevel >= LogLevelEnum.DEBUG)
      this.json({
        level: LogLevelEnum.DEBUG,
        message: LogUtils.Format(format, ...optionalParams),
      });
  }

  with(field: string, value: any): InnerJsonLog {
    const log = new InnerJsonLog(this);
    return log.with(field, value);
  }

  json(param: any) {
    param['app'] = this.app;
    param['scope'] = this.scope;
    param['timestamp'] = new Date().toISOString();

    if (param['level'] === undefined || !(param['level'] in LogLevelEnum))
      param['level'] = LogLevelEnum.LOG;

    if (this.uid) param['traceID'] = this.uid;

    if (this.logLevel >= param['level']) {
      param['level'] = LogLevelEnum[param['level']];
      if (this.worker) this.worker.postMessage(param);
      else {
        process.stdout.write(LogUtils.SafeStringify(param)+'\n');
      }
    }
  }
}

export const Logger: LoggerInterface = new JsonLogger(process.env.APP, process.env.SCOPE, null, process.env.LOG_LEVEL ?? 'LOG' );
