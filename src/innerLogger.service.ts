import { Injectable, LoggerService } from '@nestjs/common';
import {JsonLoggerInterface, LoggerInterface, LogLevelEnum} from "./logger.service";
import { LogUtils } from "./logUtils";

@Injectable()
export class InnerJsonLog implements LoggerInterface {
  private readonly fields: {};

  constructor(private _logger: JsonLoggerInterface) {
    this.fields = [];
  }

  with(field: string, value: any): InnerJsonLog {
    this.fields[field] = LogUtils.Sanitize(value);
    return this;
  }

  log(format: string, ...optionalParams: any[]) {
    this._logger.json({
      level: LogLevelEnum.LOG,
      message: LogUtils.Format(format, ...optionalParams),
      fields: { ...this.fields },
    });
  }

  error(format: string, ...optionalParams: any[]) {
    this._logger.json({
      level: LogLevelEnum.ERROR,
      message: LogUtils.Format(format, ...optionalParams),
      fields: { ...this.fields },
    });
  }

  warn(format: string, ...optionalParams: any[]) {
    this._logger.json({
      level: LogLevelEnum.WARN,
      message: LogUtils.Format(format, ...optionalParams),
      fields: { ...this.fields },
    });
  }

  debug?(format: string, ...optionalParams: any[]) {
    this._logger.json({
      level: LogLevelEnum.DEBUG,
      message: LogUtils.Format(format, ...optionalParams),
      fields: { ...this.fields },
    });
  }
}
